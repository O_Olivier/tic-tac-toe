#include "tictactoe.h"

board create_board()
{
  board b;
  b.size = BOARD_SIZE;
  b.t = (char **) malloc(b.size * sizeof(char *));
  for(short i=0; i<b.size; i++){
    b.t[i] = (char *) malloc(b.size);
    for(short j=0; j<b.size; j++){
      b.t[i][j] = EMPTY;
    }
  }
  return b;
}

void free_board(board *b)
{
  for(short i=0; i < b->size; i++)
    free(b->t[i]);
  free(b->t);
}

void print_symbol(char c)
{
  switch(c){
  case EMPTY: printf(" "); break;
  case O:     printf("O"); break;
  case X:     printf("X"); break;
  default: fprintf(stderr,"Unexpected character : %c\n",c);
  }
}

void print_board(board *b)
{
  short i,j;
  for(i=0; i < b->size; i++){
    for(j=0; j < b->size - 1; j++){
      print_symbol(b->t[i][j]);
      printf("|");
    }
    print_symbol(b->t[i][j]);
    printf("\n");
  }
}

move ask_move(board *b, int cur_player)
{
  printf("Choose a move in [i j] format : ");
  move m;
  int result = scanf("%d %d",&(m.i),&(m.j));
  
  if(!result) {
    printf("Unexpected input, try again !\n");
    return ask_move(b,cur_player);
  }
  
  return m;
}

bool valid_move(board *b, move m)
{
  return (m.i >= 0 && m.i < b->size
	  && m.j >= 0 && m.j < b->size
	  && b->t[m.i][m.j] == EMPTY);
}

board *play_move(board *b, move m, char c)
{
  assert(valid_move(b,m));
  b->t[m.i][m.j] = c;
  return b;
}

int diagonal(char **t, int size)
{
  short i;
  int b = true;
  char c;

  c = t[0][0];
  
  if(c == EMPTY) b = false;
  
  for(i=0; i<size; i++){
    if(c != t[i][i]){
      b = false;
      break;
    }
  }
  if(b) return true;

  c = t[size-1][size-1];
  
  if(c == EMPTY) return false;
  
  for(i=0; i<size; i++)
    if(c != t[i][size-1-i])
      return false;

  return true;    
}

int vertical(char **t, int size)
{
  short i,j;
  int b = true;
  char c;
  
  for(j=0; j<size; j++){
    c = t[0][j];

    if(c == EMPTY){
      b = false;
      break;
    }
    
    for(i=0; i<size; i++){
      if(c != t[i][j]){
	b = false;
	break;
      }
    }
    if(b) return true;
    b = false;
  }
  
  return false;
}

int horizontal(char **t, int size)
{
  short i,j;
  int b = true;
  char c;
  
  for(i=0; i<size; i++){
    c = t[i][0];
    
    if(c == EMPTY){
      b = false;
      break;
    }
    
    for(j=0; j<size; j++){
      if(c != t[i][j]){
	b = false;
	break;
      }
    }
    if(b) return true;
    b = false;
  }
  
  return false;
}

bool complete_board(board *b)
{
  for(short i=0; i<b->size; i++)
    for(short j=0; j<b->size; j++)
      if(b->t[i][j] == EMPTY)
	return false;
  return true;
}


status end_game(board *b)
{
  if(diagonal(b->t, b->size)
     || vertical(b->t, b->size)
     || horizontal(b->t, b->size))
    return VICTORY;
  
  else if(complete_board(b))
    return COMPLETE;

  return PLAYING;
}

void print_victory_msg(int winner)
{
  if(winner == PLAYER1)
    printf("Congratulations : PLAYER 1 won !\n");
  else if(winner == PLAYER2)
    printf("Congratulations : PLAYER 2 won !\n");
}

int next_player(int cur_player)
{
  if(cur_player == PLAYER1)
    return PLAYER2;
  return PLAYER1;
}

int previous_player(int cur_player)
{
  return next_player(cur_player);
}

int game_loop(next_move_fun f)
{
  board b = create_board();
  int current_player = PLAYER1;
  move m;
  char c;
  status st;
  int res;
  
  while((st = end_game(&b)) == PLAYING){
    printf("-- PLAYER %d --\n", current_player == PLAYER1? 1 : 2 );
    print_board(&b);
    
    while(! (valid_move (&b, m = f(&b,current_player))))
      printf("Invalid move, try again !\n");

    c = current_player == PLAYER1 ? O : X;
    b = *play_move(&b,m,c);
    current_player = next_player(current_player);
    printf("\n");
  }


  if(st == VICTORY){
    int p = previous_player(current_player);
    print_victory_msg(p);
    res = p;
  } else {
    printf("Congratulations : nobody won !\n");
    res = 0;
  }
    
  free_board(&b);

  return res;
}

int pvp_game_loop()
{
  return game_loop(&ask_move);
}

int main()
{
  pvp_game_loop();
  return 0;
}
