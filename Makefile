all: c

c: tictactoe.o
	gcc -c -Wall tictactoe.c
	gcc tictactoe.o -o tictactoe

clean:
	rm -f *~

proper:
	rm -f *.o tictactoe
