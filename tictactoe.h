#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#define BOARD_SIZE 3

//PLAYER 1 symbol is O
//PLAYER 2 symbol is X

#define EMPTY 0
#define O     1
#define X     2

#define PLAYER1 1
#define PLAYER2 2

//Game status
#define PLAYING 0
#define VICTORY 1
#define COMPLETE 2

typedef struct {
  int size;
  char **t;
} board;

typedef struct {
  int i,j;
} move;

typedef int status;

typedef move (*next_move_fun)(board *,int);

bool complete_board(board *b);

int game_loop (next_move_fun);

int pvp_game_loop();
